package com.example.demo.controller;

import com.example.demo.model.TourPackage;
import com.example.demo.repository.PackageTourRepo;
import com.example.demo.repository.TourRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TourController {

    @Autowired
    PackageTourRepo packageTourRepo;


@Autowired
TourRepo tourRepo;


    @RequestMapping(value = "/tourpackage", method = RequestMethod.GET)
    public String getTourpack(ModelMap model){
        TourPackage tourPackage = new TourPackage();
        List<TourPackage>  tourPackages = packageTourRepo.findAll();
        model.addAttribute("package", tourPackage);
        model.addAttribute("tourspackages", tourPackages);
        return "tourpackage";
    }




}
