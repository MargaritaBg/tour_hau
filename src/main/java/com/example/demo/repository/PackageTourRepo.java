package com.example.demo.repository;


import com.example.demo.model.TourPackage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PackageTourRepo extends JpaRepository<TourPackage,Integer> {



}
